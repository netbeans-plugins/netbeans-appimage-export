# netbeans-appimage-export

NetBeans plug-in for exporting an executable 64-bit Linux AppImage package of a Java application project.

#### [Download binary releases here](https://gitlab.com/netbeans-plugins/netbeans-appimage-export/tags/Release)

![Selection_031](/uploads/6e83b73fb809d093d5a9aa765bba926b/Selection_031.png)
![Selection_035](/uploads/d2c2816ad681775ef73a819a8ddccb52/Selection_035.png)