/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ide.export.appimage;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import javax.swing.table.DefaultTableModel;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author user
 */
public class ExportAppImageDialog extends javax.swing.JDialog {

    private Properties properties;
    private Properties platform_properties;

    /**
     * Creates new form ExportAppImageForm
     */
    public ExportAppImageDialog(Properties properties, Properties platform_properties) {
        this.properties = properties;
        this.platform_properties = platform_properties;
        initComponents();
        populateConfiguration();
    }

    private void populateConfiguration() {
        txt_application_name.setText(properties.getProperty("application.title"));
        txt_build_version.setText(properties.getProperty("build.version"));
        txt_main_class.setText(properties.getProperty("main.class"));
        txt_output_directory.setText(properties.getProperty("appimage.output.dir"));
        cb_jvm_integration.setSelectedItem(properties.getProperty("jvm.integration"));
        txt_jvm_args.setText(properties.getProperty("run.jvmargs"));
        txt_app_args.setText(properties.getProperty("application.args"));
        JSONArray env_vars = new JSONArray(properties.getProperty("env.vars"));
        if (env_vars.length() > 0) {
            for (JSONObject item : env_vars.toArray(JSONObject.class)) {
                ((DefaultTableModel) tb_env_vars.getModel()).addRow(new String[]{item.getString("key"), item.getString("value")});
            }
        }
    }

    private void buildProject() {
        SwingWorker worker = new SwingWorker() {
            @Override
            protected Object doInBackground() throws Exception {
                try {
                    txt_output_log.setText("Building Java project..\n");
                    Process ant_build_project = Runtime.getRuntime().exec(platform_properties.getProperty("ant.runtime.path") + " -f " + platform_properties.getProperty("build.file.path") + " jar");
                    ant_build_project.waitFor();

                    txt_output_log.setText(txt_output_log.getText() + "Preparing build dependencies..\n");
                    String appimage_dir = platform_properties.getProperty("dist.dir") + System.getProperty("file.separator") + "appimage";
                    FileUtils.deleteDirectory(new File(appimage_dir));
                    
                    txt_output_log.setText(txt_output_log.getText() + "Copying .jar libraries from /lib project directory..\n");
                    if (new File(platform_properties.getProperty("dist.dir") + "/../lib").isDirectory()) {
                        FileUtils.copyDirectory(new File(platform_properties.getProperty("dist.dir") + "/../lib"), new File(appimage_dir + "/usr/lib"));
                    }
                    
                    FileUtils.copyFileToDirectory(new File(platform_properties.getProperty("dist.jar")), new File(appimage_dir + "/usr/lib"));

                    txt_output_log.setText(txt_output_log.getText() + "Generating start-up script..\n");
                    String shell_script = "#!/bin/bash\n\n"
                            + "BASE_DIR=\"$( cd \"$(dirname \"$0\")\" ; pwd -P )\"\n\n"
                            + "cd $BASE_DIR\n\n";
                    for (int i = 0; i < ((DefaultTableModel) tb_env_vars.getModel()).getRowCount(); i++) {
                        String key = (String) ((DefaultTableModel) tb_env_vars.getModel()).getValueAt(i, 0);
                        String value = (String) ((DefaultTableModel) tb_env_vars.getModel()).getValueAt(i, 1);
                        if (key.trim().length() > 0 && value.trim().length() > 0) {
                            shell_script = shell_script + "export " + key + "=\"" + value + "\"" + "\n";
                        }
                    }
                    if (String.valueOf(cb_jvm_integration.getSelectedItem()).trim().equalsIgnoreCase("JRE")) {
                        FileUtils.copyDirectory(new File(System.getProperty("java.home")), new File(appimage_dir + System.getProperty("file.separator") + "jre"));
                        shell_script = shell_script + "export JAVA_HOME=$BASE_DIR/jre\n";
                        shell_script = shell_script + "export PATH=$JAVA_HOME/bin:$PATH\n";
                        shell_script = shell_script + "\n$BASE_DIR/../../jre/bin/java " + txt_jvm_args.getText() + " -cp \"../lib/*\" \"" + txt_main_class.getText() + "\" " + txt_app_args.getText() + " \"$@\"\n";
                    } else if (String.valueOf(cb_jvm_integration.getSelectedItem()).trim().equalsIgnoreCase("JDK")) {
                        FileUtils.copyDirectoryToDirectory(new File(System.getProperty("java.home").substring(0, System.getProperty("java.home").length() - 3) + "bin"), new File(appimage_dir + System.getProperty("file.separator") + "jdk"));
                        FileUtils.copyDirectoryToDirectory(new File(System.getProperty("java.home").substring(0, System.getProperty("java.home").length() - 3) + "include"), new File(appimage_dir + System.getProperty("file.separator") + "jdk"));
                        FileUtils.copyDirectoryToDirectory(new File(System.getProperty("java.home").substring(0, System.getProperty("java.home").length() - 3) + "jre"), new File(appimage_dir + System.getProperty("file.separator") + "jdk"));
                        FileUtils.copyDirectoryToDirectory(new File(System.getProperty("java.home").substring(0, System.getProperty("java.home").length() - 3) + "lib"), new File(appimage_dir + System.getProperty("file.separator") + "jdk"));
                        shell_script = shell_script + "export JAVA_HOME=$BASE_DIR/jdk\n";
                        shell_script = shell_script + "export PATH=$JAVA_HOME/bin:$PATH\n";
                        shell_script = shell_script + "\n$BASE_DIR/../../jdk/bin/java " + txt_jvm_args.getText() + " -cp \"../lib/*\" \"" + txt_main_class.getText() + "\" " + txt_app_args.getText() + " \"$@\"\n";
                    } else {
                        shell_script = shell_script + "\n" + "java " + txt_jvm_args.getText() + " -cp \"../lib/*\" \"" + txt_main_class.getText() + "\" " + txt_app_args.getText() + " \"$@\"\n";
                    }

                    txt_output_log.setText(txt_output_log.getText() + "Preparing AppImage compile dependencies..\n");
                    FileUtils.writeStringToFile(new File(appimage_dir + "/usr/bin/run"), shell_script, Charset.forName("UTF8"));
                    FileUtils.copyFile(new File(platform_properties.getProperty("appimage.icon.path")), new File(appimage_dir + "/" + properties.getProperty("application.title") + ".png"));
                    FileUtils.copyFile(new File(platform_properties.getProperty("appimage.runner.path")), new File(appimage_dir + "/AppRun"));
                    String desktop_file = "[Desktop Entry]\n"
                            + "Type=Application\n"
                            + "Encoding=UTF-8\n"
                            + "Terminal=false\n"
                            + "Name=" + properties.getProperty("application.title") + "\n"
                            + "Icon=" + properties.getProperty("application.title") + "\n"
                            + "GenericName=" + properties.getProperty("application.title") + "\n"
                            + "Comment=" + properties.getProperty("application.title") + "\n"
                            + "Exec=run\n";
                    FileUtils.writeStringToFile(new File(appimage_dir + "/" + properties.getProperty("application.title") + ".desktop"), desktop_file, Charset.forName("UTF8"));
                    FileUtils.writeStringToFile(new File(appimage_dir + "/.DirIcon"), properties.getProperty("application.title") + ".png", Charset.forName("UTF8"));

                    txt_output_log.setText(txt_output_log.getText() + "Setting file rights..\n");
                    Process process_file_rights = Runtime.getRuntime().exec("chmod 777 -R " + appimage_dir);
                    process_file_rights.waitFor();

                    txt_output_log.setText(txt_output_log.getText() + "Compiling AppImage archive..\n");
                    String appimage_file_name = txt_application_name.getText();
                    if (txt_build_version.getText().trim().length() > 0) {
                        appimage_file_name = appimage_file_name + "-" + txt_build_version.getText().trim();
                    }
                    appimage_file_name = appimage_file_name + "-x86_64.AppImage";
                    Process compile_appimage = Runtime.getRuntime().exec(platform_properties.getProperty("appimage.compiler.path") + " " + appimage_dir + " " + platform_properties.getProperty("dist.dir") + "/" + appimage_file_name);
                    compile_appimage.waitFor();

                    txt_output_log.setText(txt_output_log.getText() + "Executing clean-up..\n");
                    FileUtils.deleteDirectory(new File(appimage_dir));

                    JOptionPane.showMessageDialog(null, "AppImage build completed succesfully");

                    if (txt_output_directory.getText().trim().length() > 0) {
                        if (new File(txt_output_directory.getText().trim()).isDirectory()) {
                            if (new File(txt_output_directory.getText().trim() + "/" + appimage_file_name).isFile()) {
                                Files.delete(Paths.get(txt_output_directory.getText().trim() + "/" + appimage_file_name));
                            }
                            FileUtils.moveFile(new File(platform_properties.getProperty("dist.dir") + "/" + appimage_file_name), new File(txt_output_directory.getText().trim() + "/" + appimage_file_name));
                        }
                    }

                    txt_output_log.setText(txt_output_log.getText() + "\nAppImage build completed!!\n");
                } catch (Exception e) {
                    txt_output_log.setText(txt_output_log.getText() + e.getMessage());
                }

                return null;
            }

            @Override
            protected void process(List chunks) {
                for (Object object : chunks) {
                    Integer progress = (Integer) object;
                }
            }
        };
        worker.execute();

    }

    private boolean validateProperties() {
        boolean output = false;
        String errors = "";
        if (txt_application_name.getText().trim().length() > 0) {
            if (StringUtils.isAlphanumeric(txt_application_name.getText())) {
                output = true;
            } else {
                output = false;
                errors = errors + "Application Name must not contain spaces and special characters.\n";
            }
        } else {
            output = false;
            errors = errors + "Application Name must not be blank.\n";
        }
        if (txt_main_class.getText().trim().length() == 0) {
            output = false;
            errors = errors + "Main Class must not be blank.\n";
        }
        if (output == false) {
            JOptionPane.showMessageDialog(null, errors, "Invalid Input", JOptionPane.ERROR_MESSAGE);
        }
        return output;
    }

    private void saveProperties() {
        if (validateProperties() == true) {
            properties.setProperty("application.title", txt_application_name.getText().trim());
            properties.setProperty("build.version", txt_build_version.getText().trim());
            properties.setProperty("main.class", txt_main_class.getText().trim());
            properties.setProperty("appimage.output.dir", txt_output_directory.getText().trim());
            properties.setProperty("jvm.integration", String.valueOf(cb_jvm_integration.getSelectedItem()));
            properties.setProperty("run.jvmargs", txt_jvm_args.getText().trim());
            properties.setProperty("application.args", txt_app_args.getText().trim());
            JSONArray env_args = new JSONArray();
            for (int i = 0; i < ((DefaultTableModel) tb_env_vars.getModel()).getRowCount(); i++) {
                String key = ((DefaultTableModel) tb_env_vars.getModel()).getValueAt(i, 0).toString();
                String value = ((DefaultTableModel) tb_env_vars.getModel()).getValueAt(i, 1).toString();
                if (key.trim().length() > 0 && value.trim().length() > 0) {
                    JSONObject item = new JSONObject();
                    item.put("key", key.trim());
                    item.put("value", value.trim());
                    env_args.put(item);
                }
            }
            properties.setProperty("env.vars", env_args.toString());
            try {
                properties.store(new FileOutputStream(new File(platform_properties.getProperty("appimage.properties.path"))), "# AppImage export configuration");
                JOptionPane.showMessageDialog(null, "Appimage properties saved succesfuly in project.");
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Error saving appimage.properties file.", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        tpnl_container = new javax.swing.JTabbedPane();
        pnl_configuration = new javax.swing.JPanel();
        lbl_application_name = new javax.swing.JLabel();
        lbl_build_version = new javax.swing.JLabel();
        txt_application_name = new javax.swing.JFormattedTextField();
        txt_build_version = new javax.swing.JFormattedTextField();
        flr_1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));
        lbl_main_class = new javax.swing.JLabel();
        txt_main_class = new javax.swing.JFormattedTextField();
        lbl_output_directory = new javax.swing.JLabel();
        txt_output_directory = new javax.swing.JFormattedTextField();
        btn_output_directory = new javax.swing.JButton();
        lbl_jvm_integration = new javax.swing.JLabel();
        cb_jvm_integration = new javax.swing.JComboBox<>();
        btn_load_app_name = new javax.swing.JButton();
        btn_load_main_class = new javax.swing.JButton();
        pnl_jvm_args = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txt_jvm_args = new javax.swing.JTextPane();
        txt_app_args = new javax.swing.JFormattedTextField();
        lbl_jvm_args = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btn_load_jvm_args = new javax.swing.JButton();
        btn_load_app_args = new javax.swing.JButton();
        pnl_environment = new javax.swing.JPanel();
        lbl_env_vars = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tb_env_vars = new javax.swing.JTable();
        btn_del_env_var = new javax.swing.JButton();
        btn_add_env_var = new javax.swing.JButton();
        pnl_build = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        txt_output_log = new javax.swing.JTextArea();
        jPanel2 = new javax.swing.JPanel();
        btn_build = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        btn_exit = new javax.swing.JButton();
        btn_save = new javax.swing.JButton();
        filler2 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 32767));

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.title")); // NOI18N
        setMinimumSize(new java.awt.Dimension(600, 440));
        setModalityType(java.awt.Dialog.ModalityType.APPLICATION_MODAL);
        setPreferredSize(new java.awt.Dimension(600, 440));

        pnl_configuration.setBorder(javax.swing.BorderFactory.createEmptyBorder(20, 20, 20, 20));
        java.awt.GridBagLayout pnl_configurationLayout = new java.awt.GridBagLayout();
        pnl_configurationLayout.columnWidths = new int[] {0, 10, 0, 10, 0};
        pnl_configurationLayout.rowHeights = new int[] {0, 10, 0, 10, 0, 10, 0, 10, 0, 10, 0};
        pnl_configuration.setLayout(pnl_configurationLayout);

        org.openide.awt.Mnemonics.setLocalizedText(lbl_application_name, org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.lbl_application_name.text")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        pnl_configuration.add(lbl_application_name, gridBagConstraints);

        org.openide.awt.Mnemonics.setLocalizedText(lbl_build_version, org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.lbl_build_version.text")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        pnl_configuration.add(lbl_build_version, gridBagConstraints);

        txt_application_name.setText(org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.txt_application_name.text")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        pnl_configuration.add(txt_application_name, gridBagConstraints);

        txt_build_version.setText(org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.txt_build_version.text")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        pnl_configuration.add(txt_build_version, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.weighty = 0.1;
        pnl_configuration.add(flr_1, gridBagConstraints);

        org.openide.awt.Mnemonics.setLocalizedText(lbl_main_class, org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.lbl_main_class.text")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        pnl_configuration.add(lbl_main_class, gridBagConstraints);

        txt_main_class.setText(org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.txt_main_class.text")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        pnl_configuration.add(txt_main_class, gridBagConstraints);

        org.openide.awt.Mnemonics.setLocalizedText(lbl_output_directory, org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.lbl_output_directory.text")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        pnl_configuration.add(lbl_output_directory, gridBagConstraints);

        txt_output_directory.setText(org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.txt_output_directory.text")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        pnl_configuration.add(txt_output_directory, gridBagConstraints);

        btn_output_directory.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ide/export/appimage/folder.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(btn_output_directory, org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.btn_output_directory.text")); // NOI18N
        btn_output_directory.setToolTipText(org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.btn_output_directory.toolTipText")); // NOI18N
        btn_output_directory.setActionCommand(org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.btn_output_directory.actionCommand")); // NOI18N
        btn_output_directory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_output_directoryActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 6;
        pnl_configuration.add(btn_output_directory, gridBagConstraints);

        org.openide.awt.Mnemonics.setLocalizedText(lbl_jvm_integration, org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.lbl_jvm_integration.text")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        pnl_configuration.add(lbl_jvm_integration, gridBagConstraints);

        cb_jvm_integration.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "NONE", "JRE", "JDK" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        pnl_configuration.add(cb_jvm_integration, gridBagConstraints);

        btn_load_app_name.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ide/export/appimage/download.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(btn_load_app_name, org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.btn_load_app_name.text")); // NOI18N
        btn_load_app_name.setToolTipText(org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.btn_load_app_name.toolTipText")); // NOI18N
        btn_load_app_name.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_load_app_nameActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        pnl_configuration.add(btn_load_app_name, gridBagConstraints);

        btn_load_main_class.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ide/export/appimage/download.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(btn_load_main_class, org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.btn_load_main_class.text")); // NOI18N
        btn_load_main_class.setToolTipText(org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.btn_load_main_class.toolTipText")); // NOI18N
        btn_load_main_class.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_load_main_classActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 4;
        pnl_configuration.add(btn_load_main_class, gridBagConstraints);

        tpnl_container.addTab(org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.pnl_configuration.TabConstraints.tabTitle"), new javax.swing.ImageIcon(getClass().getResource("/ide/export/appimage/appimage.png")), pnl_configuration, org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.pnl_configuration.TabConstraints.tabToolTip")); // NOI18N

        jScrollPane1.setViewportView(txt_jvm_args);

        txt_app_args.setText(org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.txt_app_args.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(lbl_jvm_args, org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.lbl_jvm_args.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(jLabel2, org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.jLabel2.text")); // NOI18N

        btn_load_jvm_args.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ide/export/appimage/download.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(btn_load_jvm_args, org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.btn_load_jvm_args.text")); // NOI18N
        btn_load_jvm_args.setToolTipText(org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.btn_load_jvm_args.toolTipText")); // NOI18N
        btn_load_jvm_args.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_load_jvm_argsActionPerformed(evt);
            }
        });

        btn_load_app_args.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ide/export/appimage/download.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(btn_load_app_args, org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.btn_load_app_args.text")); // NOI18N
        btn_load_app_args.setToolTipText(org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.btn_load_app_args.toolTipText")); // NOI18N
        btn_load_app_args.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_load_app_argsActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnl_jvm_argsLayout = new javax.swing.GroupLayout(pnl_jvm_args);
        pnl_jvm_args.setLayout(pnl_jvm_argsLayout);
        pnl_jvm_argsLayout.setHorizontalGroup(
            pnl_jvm_argsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_jvm_argsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl_jvm_argsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_app_args)
                    .addGroup(pnl_jvm_argsLayout.createSequentialGroup()
                        .addComponent(lbl_jvm_args, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(btn_load_jvm_args))
                    .addComponent(jScrollPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnl_jvm_argsLayout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 218, Short.MAX_VALUE)
                        .addComponent(btn_load_app_args)))
                .addContainerGap())
        );
        pnl_jvm_argsLayout.setVerticalGroup(
            pnl_jvm_argsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_jvm_argsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl_jvm_argsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btn_load_jvm_args)
                    .addComponent(lbl_jvm_args))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(pnl_jvm_argsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btn_load_app_args)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_app_args, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tpnl_container.addTab(org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.pnl_jvm_args.TabConstraints.tabTitle"), new javax.swing.ImageIcon(getClass().getResource("/ide/export/appimage/cup.png")), pnl_jvm_args, org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.pnl_jvm_args.TabConstraints.tabToolTip")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(lbl_env_vars, org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.lbl_env_vars.text")); // NOI18N

        tb_env_vars.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name", "Value"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        tb_env_vars.setColumnSelectionAllowed(true);
        tb_env_vars.setFillsViewportHeight(true);
        jScrollPane3.setViewportView(tb_env_vars);
        tb_env_vars.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        if (tb_env_vars.getColumnModel().getColumnCount() > 0) {
            tb_env_vars.getColumnModel().getColumn(0).setHeaderValue(org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.tb_env_vars.columnModel.title0")); // NOI18N
            tb_env_vars.getColumnModel().getColumn(1).setHeaderValue(org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.tb_env_vars.columnModel.title1")); // NOI18N
        }

        btn_del_env_var.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ide/export/appimage/delete.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(btn_del_env_var, org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.btn_del_env_var.text")); // NOI18N
        btn_del_env_var.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_del_env_varActionPerformed(evt);
            }
        });

        btn_add_env_var.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ide/export/appimage/add.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(btn_add_env_var, org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.btn_add_env_var.text")); // NOI18N
        btn_add_env_var.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_add_env_varActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnl_environmentLayout = new javax.swing.GroupLayout(pnl_environment);
        pnl_environment.setLayout(pnl_environmentLayout);
        pnl_environmentLayout.setHorizontalGroup(
            pnl_environmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_environmentLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl_environmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 434, Short.MAX_VALUE)
                    .addGroup(pnl_environmentLayout.createSequentialGroup()
                        .addComponent(lbl_env_vars, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_add_env_var)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_del_env_var)))
                .addContainerGap())
        );
        pnl_environmentLayout.setVerticalGroup(
            pnl_environmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_environmentLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl_environmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pnl_environmentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(btn_del_env_var)
                        .addComponent(btn_add_env_var))
                    .addComponent(lbl_env_vars))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)
                .addContainerGap())
        );

        tpnl_container.addTab(org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.pnl_environment.TabConstraints.tabTitle"), new javax.swing.ImageIcon(getClass().getResource("/ide/export/appimage/environment.png")), pnl_environment, org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.pnl_environment.TabConstraints.tabToolTip")); // NOI18N

        txt_output_log.setEditable(false);
        txt_output_log.setColumns(20);
        txt_output_log.setRows(5);
        jScrollPane4.setViewportView(txt_output_log);

        jPanel2.setLayout(new java.awt.GridLayout(1, 0));

        btn_build.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ide/export/appimage/build.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(btn_build, org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.btn_build.text")); // NOI18N
        btn_build.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_buildActionPerformed(evt);
            }
        });
        jPanel2.add(btn_build);

        javax.swing.GroupLayout pnl_buildLayout = new javax.swing.GroupLayout(pnl_build);
        pnl_build.setLayout(pnl_buildLayout);
        pnl_buildLayout.setHorizontalGroup(
            pnl_buildLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnl_buildLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnl_buildLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 434, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnl_buildLayout.setVerticalGroup(
            pnl_buildLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnl_buildLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 228, Short.MAX_VALUE)
                .addContainerGap())
        );

        tpnl_container.addTab(org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.pnl_build.TabConstraints.tabTitle"), new javax.swing.ImageIcon(getClass().getResource("/ide/export/appimage/tux.png")), pnl_build, org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.pnl_build.TabConstraints.tabToolTip")); // NOI18N

        tpnl_container.setSelectedComponent(pnl_configuration);

        btn_exit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ide/export/appimage/exit.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(btn_exit, org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.btn_exit.text")); // NOI18N
        btn_exit.setToolTipText(org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.btn_exit.toolTipText")); // NOI18N
        btn_exit.setMinimumSize(new java.awt.Dimension(100, 31));
        btn_exit.setPreferredSize(new java.awt.Dimension(200, 31));
        btn_exit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_exitActionPerformed(evt);
            }
        });

        btn_save.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ide/export/appimage/save.png"))); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(btn_save, org.openide.util.NbBundle.getMessage(ExportAppImageDialog.class, "ExportAppImageDialog.btn_save.text")); // NOI18N
        btn_save.setPreferredSize(new java.awt.Dimension(200, 31));
        btn_save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_saveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(filler2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(btn_exit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_save, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(filler2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_exit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_save, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tpnl_container)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tpnl_container)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btn_add_env_varActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_add_env_varActionPerformed
        ((DefaultTableModel) tb_env_vars.getModel()).addRow(new String[]{"", ""});
    }//GEN-LAST:event_btn_add_env_varActionPerformed

    private void btn_del_env_varActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_del_env_varActionPerformed
        if (tb_env_vars.getSelectedRowCount() > 0) {
            ((DefaultTableModel) tb_env_vars.getModel()).removeRow(tb_env_vars.getSelectedRow());
        }
    }//GEN-LAST:event_btn_del_env_varActionPerformed

    private void btn_load_app_argsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_load_app_argsActionPerformed
        txt_app_args.setText(platform_properties.getProperty("application.args"));
    }//GEN-LAST:event_btn_load_app_argsActionPerformed

    private void btn_load_jvm_argsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_load_jvm_argsActionPerformed
        txt_jvm_args.setText(platform_properties.getProperty("run.jvmargs"));
    }//GEN-LAST:event_btn_load_jvm_argsActionPerformed

    private void btn_load_app_nameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_load_app_nameActionPerformed
        txt_application_name.setText(platform_properties.getProperty("application.title"));
    }//GEN-LAST:event_btn_load_app_nameActionPerformed

    private void btn_load_main_classActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_load_main_classActionPerformed
        txt_main_class.setText(platform_properties.getProperty("main.class"));
    }//GEN-LAST:event_btn_load_main_classActionPerformed

    private void btn_output_directoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_output_directoryActionPerformed
        JFileChooser dialog = new JFileChooser();
        dialog.setCurrentDirectory(new File("."));
        dialog.setDialogTitle("Choose Output Directory");
        dialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        dialog.setAcceptAllFileFilterUsed(false);
        if (dialog.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            txt_output_directory.setText(dialog.getSelectedFile().getAbsolutePath());
        }
    }//GEN-LAST:event_btn_output_directoryActionPerformed

    private void btn_saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_saveActionPerformed
        saveProperties();
    }//GEN-LAST:event_btn_saveActionPerformed

    private void btn_exitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_exitActionPerformed
        dispose();
    }//GEN-LAST:event_btn_exitActionPerformed

    private void btn_buildActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_buildActionPerformed
        buildProject();
    }//GEN-LAST:event_btn_buildActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_add_env_var;
    private javax.swing.JButton btn_build;
    private javax.swing.JButton btn_del_env_var;
    private javax.swing.JButton btn_exit;
    private javax.swing.JButton btn_load_app_args;
    private javax.swing.JButton btn_load_app_name;
    private javax.swing.JButton btn_load_jvm_args;
    private javax.swing.JButton btn_load_main_class;
    private javax.swing.JButton btn_output_directory;
    private javax.swing.JButton btn_save;
    private javax.swing.JComboBox<String> cb_jvm_integration;
    private javax.swing.Box.Filler filler2;
    private javax.swing.Box.Filler flr_1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JLabel lbl_application_name;
    private javax.swing.JLabel lbl_build_version;
    private javax.swing.JLabel lbl_env_vars;
    private javax.swing.JLabel lbl_jvm_args;
    private javax.swing.JLabel lbl_jvm_integration;
    private javax.swing.JLabel lbl_main_class;
    private javax.swing.JLabel lbl_output_directory;
    private javax.swing.JPanel pnl_build;
    private javax.swing.JPanel pnl_configuration;
    private javax.swing.JPanel pnl_environment;
    private javax.swing.JPanel pnl_jvm_args;
    private javax.swing.JTable tb_env_vars;
    private javax.swing.JTabbedPane tpnl_container;
    private javax.swing.JFormattedTextField txt_app_args;
    private javax.swing.JFormattedTextField txt_application_name;
    private javax.swing.JFormattedTextField txt_build_version;
    private javax.swing.JTextPane txt_jvm_args;
    private javax.swing.JFormattedTextField txt_main_class;
    private javax.swing.JFormattedTextField txt_output_directory;
    private javax.swing.JTextArea txt_output_log;
    // End of variables declaration//GEN-END:variables
}
