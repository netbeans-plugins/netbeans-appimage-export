/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ide.export.appimage;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Properties;
import java.util.UUID;
import javax.swing.JDialog;
import org.netbeans.api.project.Project;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle.Messages;

@ActionID(
        category = "Project",
        id = "ide.export.appimage.ExportAppImageAction"
)
@ActionRegistration(
        displayName = "Export Project to AppImage",
        menuText = "To AppImage",
        iconBase = "ide/export/appimage/appimage.png",
        iconInMenu = false        
)
@ActionReferences({
    @ActionReference(path = "Menu/File/Export", position = 0, name = "To AppImage"),
    @ActionReference(path = "Toolbars/Build", position = 500, name = "Export Project to AppImage")
})
@Messages("CTL_ExportAppImageAction=Export Project to AppImage")
public final class ExportAppImageAction implements ActionListener {

    private final Project context;
    private String project_path;
    private String project_properties_path;
    private String project_private_properties_path;
    private String appimage_properties_path;
    private String platform_path;
    private String user_root_path;
    private String ant_runtime_path;
    private String build_file_path;
    private String appimage_compiler_path;
    private String appimage_runner_path;
    private String appimage_dir_path;
    private String appimage_icon_path;

    public ExportAppImageAction(Project context) {
        this.context = context;
        platform_path = System.getProperty("netbeans.home");
        user_root_path = System.getProperty("netbeans.default_userdir_root");
        project_path = this.context.getProjectDirectory().getPath();
        project_properties_path = project_path + System.getProperty("file.separator") + "nbproject" + System.getProperty("file.separator") + "project.properties";
        project_private_properties_path = project_path + System.getProperty("file.separator") + "nbproject" + System.getProperty("file.separator") + "private" + System.getProperty("file.separator") + "private.properties";
        appimage_properties_path = project_path + System.getProperty("file.separator") + "nbproject" + System.getProperty("file.separator") + "appimage.properties";
        ant_runtime_path = platform_path.substring(0, platform_path.length() - 8) + "extide" + System.getProperty("file.separator") + "ant" + System.getProperty("file.separator") + "bin" + System.getProperty("file.separator") + "ant";
        build_file_path = project_path + System.getProperty("file.separator") + "nbproject" + System.getProperty("file.separator") + "build-impl.xml";
        appimage_dir_path = user_root_path + System.getProperty("file.separator") + "appimage";
        appimage_compiler_path = appimage_dir_path + System.getProperty("file.separator") + "appimagetool";
        appimage_runner_path = appimage_dir_path + System.getProperty("file.separator") + "AppRun-x86_64";
        appimage_icon_path = appimage_dir_path + System.getProperty("file.separator") + "icon.png";
        try {
            validateAssets();
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    private void validateAssets() throws IOException {
        if (!new File(appimage_dir_path).isDirectory()) {
            Files.createDirectory(Paths.get(appimage_dir_path));
        }

        if (!new File(appimage_icon_path).exists()) {
            Files.copy(getFileFromInputStream(getClass().getResourceAsStream("/ide/export/appimage/assets/icon.png")).toPath(), Paths.get(appimage_icon_path));
            Files.setPosixFilePermissions(Paths.get(appimage_icon_path), PosixFilePermissions.fromString("rwxrwxrwx"));
        }

        if (!new File(appimage_compiler_path).exists()) {
            Files.copy(getFileFromInputStream(getClass().getResourceAsStream("/ide/export/appimage/assets/appimagetool")).toPath(), Paths.get(appimage_compiler_path));
            Files.setPosixFilePermissions(Paths.get(appimage_compiler_path), PosixFilePermissions.fromString("rwxrwxrwx"));
        }

        if (!new File(appimage_runner_path).exists()) {
            Files.copy(getFileFromInputStream(getClass().getResourceAsStream("/ide/export/appimage/assets/AppRun-x86_64")).toPath(), Paths.get(appimage_runner_path));
            Files.setPosixFilePermissions(Paths.get(appimage_runner_path), PosixFilePermissions.fromString("rwxrwxrwx"));
        }
    }

    public static File getFileFromInputStream(InputStream input_stream) throws IOException {
        File output = File.createTempFile(UUID.randomUUID().toString(), ".tmp");
        output.deleteOnExit();
        FileOutputStream output_stream = new FileOutputStream(output);
        copyInputStreamToOutputStream(input_stream, output_stream);
        output_stream.close();
        return output;
    }

    public static void copyInputStreamToOutputStream(InputStream input_stream, OutputStream output_stream) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = input_stream.read(buffer)) != -1) {
            output_stream.write(buffer, 0, read);
        }
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        try {
            Properties properties_file_data = new Properties();
            properties_file_data.load(new DataInputStream(new FileInputStream(project_properties_path)));

            Properties private_properties_file_data = new Properties();
            private_properties_file_data.load(new DataInputStream(new FileInputStream(project_private_properties_path)));

            Properties platform_properties = new Properties();
            platform_properties.setProperty("platform.path", platform_path);
            platform_properties.setProperty("ant.runtime.path", ant_runtime_path);
            platform_properties.setProperty("build.file.path", build_file_path);
            platform_properties.setProperty("appimage.properties.path", appimage_properties_path);
            platform_properties.setProperty("appimage.dir.path", appimage_dir_path);
            platform_properties.setProperty("appimage.compiler.path", appimage_compiler_path);
            platform_properties.setProperty("appimage.runner.path", appimage_runner_path);
            platform_properties.setProperty("appimage.icon.path", appimage_icon_path);
            platform_properties.setProperty("dist.dir", project_path + System.getProperty("file.separator") + properties_file_data.getProperty("dist.dir"));
            platform_properties.setProperty("dist.jar", properties_file_data.getProperty("dist.jar").replace("${dist.dir}", platform_properties.getProperty("dist.dir")));
            if(properties_file_data.containsKey("run.jvmargs")){
                platform_properties.setProperty("run.jvmargs", properties_file_data.getProperty("run.jvmargs"));
            }else{
                platform_properties.setProperty("run.jvmargs", "");
            }            
            if(private_properties_file_data.containsKey("application.args")){
                platform_properties.setProperty("application.args", private_properties_file_data.getProperty("application.args"));
            }else{
                platform_properties.setProperty("application.args", "");
            }
            if(properties_file_data.containsKey("application.title")){
                platform_properties.setProperty("application.title", properties_file_data.getProperty("application.title"));
            }else{
                platform_properties.setProperty("application.title", project_path.split(System.getProperty("file.separator"))[project_path.split(System.getProperty("file.separator")).length - 1].trim());
            }
            if(properties_file_data.containsKey("main.class")){
                platform_properties.setProperty("main.class", properties_file_data.getProperty("main.class"));
            }else{
                platform_properties.setProperty("main.class", "");
            }
            
            Properties properties;
            if (new File(appimage_properties_path).isFile()) {
                properties = new Properties();
                properties.load(new DataInputStream(new FileInputStream(appimage_properties_path)));
                if (!properties.containsKey("application.title")) {
                    if (properties_file_data.containsKey("application.title")) {
                        properties.setProperty("application.title", properties_file_data.getProperty("application.title"));
                    } else {
                        properties.setProperty("application.title", project_path.split(System.getProperty("file.separator"))[project_path.split(System.getProperty("file.separator")).length - 1].trim());
                    }
                }
                if (!properties.containsKey("appimage.output.dir")) {
                    properties.setProperty("appimage.output.dir", project_path + System.getProperty("file.separator") + properties_file_data.getProperty("dist.dir") + System.getProperty("file.separator") + "AppImage");
                }
                if (!properties.containsKey("run.jvmargs")) {
                    if (properties_file_data.containsKey("run.jvmargs")) {
                        properties.setProperty("run.jvmargs", properties_file_data.getProperty("run.jvmargs"));
                    } else {
                        properties.setProperty("run.jvmargs", "");
                    }
                }
                if (!properties.containsKey("main.class")) {
                    if (properties_file_data.containsKey("main.class")) {
                        properties.setProperty("main.class", properties_file_data.getProperty("main.class"));
                    } else {
                        properties.setProperty("main.class", "");
                    }
                }
                if (!properties.containsKey("application.args")) {
                    if (private_properties_file_data.containsKey("application.args")) {
                        properties.setProperty("application.args", private_properties_file_data.getProperty("application.args"));
                    } else {
                        properties.setProperty("application.args", "");
                    }
                }
                if (!properties.containsKey("build.version")) {
                    properties.setProperty("build.version", "0.0.1");
                }
                if (!properties.containsKey("jvm.integration")) {
                    properties.setProperty("jvm.integration", "NONE");
                }
                if(!properties.containsKey("env.vars")){
                    properties.setProperty("env.vars", "[]");
                }
            } else {
                File file = new File(appimage_properties_path);
                properties = new Properties();
                if (properties_file_data.containsKey("application.title")) {
                    properties.setProperty("application.title", properties_file_data.getProperty("application.title"));
                } else {
                    properties.setProperty("application.title", project_path.split(System.getProperty("file.separator"))[project_path.split(System.getProperty("file.separator")).length - 1].trim());
                }
                properties.setProperty("appimage.output.dir", project_path + System.getProperty("file.separator") + properties_file_data.getProperty("dist.dir") + System.getProperty("file.separator") + "AppImage");
                if (properties_file_data.containsKey("run.jvmargs")) {
                    properties.setProperty("run.jvmargs", properties_file_data.getProperty("run.jvmargs"));
                } else {
                    properties.setProperty("run.jvmargs", "");
                }
                if (properties_file_data.containsKey("main.class")) {
                    properties.setProperty("main.class", properties_file_data.getProperty("main.class"));
                } else {
                    properties.setProperty("main.class", "");
                }
                if (private_properties_file_data.containsKey("application.args")) {
                    properties.setProperty("application.args", private_properties_file_data.getProperty("application.args"));
                } else {
                    properties.setProperty("application.args", "");
                }
                properties.setProperty("build.version", "0.0.1");
                properties.setProperty("jvm.integration", "NONE");
                properties.setProperty("env.vars", "[]");

                properties.store(new FileOutputStream(file), "# AppImage export configuration");

            }

            JDialog window = new ExportAppImageDialog(properties, platform_properties);
            window.setLocationRelativeTo(null);
            window.setVisible(true);
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
    }
}
